﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AVXCalculator
{
    public class ResultsVM : INotifyPropertyChanged
    {
        private readonly Action<NamedDataBase> m_NewVectorCallback;
        private NamedDataBase m_ComputedResult;
        private bool m_LastWasError;
        private string m_LastComputed;
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        //called when a computation failed, so that we display an error message
        public void MarkAsError()
        {
            m_LastWasError = true;

            //notify that the displayed string has changed
            NotifyPropertyChanged("ResultString");
        }

        public string ResultString
        {
            get
            {
                if (m_LastWasError)
                {
                    return "PROGRAMMING ERROR";
                }
                else if (m_ComputedResult != null)
                {
                    return m_ComputedResult.ToString();
                }
                else
                {
                    return "";
                }
            }
        }
        public NamedDataBase ResultValue
        {
            get { return m_ComputedResult; }
            set
            {
                m_ComputedResult = value;
                m_NewVectorCallback(value);

                m_LastWasError = false;

                NotifyPropertyChanged();
                NotifyPropertyChanged("ResultString");
            }
        }

        public string LastComputation
        {
            get { return m_LastComputed; }
            set
            {
                m_LastComputed = value;
                NotifyPropertyChanged();
            }
        }

        public ResultsVM(Action<NamedDataBase> newVectorCallback)
        {
            m_NewVectorCallback = newVectorCallback;
        }
    }
}

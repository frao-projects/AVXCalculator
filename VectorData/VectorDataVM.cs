﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AVXCalculator
{
    public class VectorDataVM<ElementType> where ElementType : NamedDataBase, INotifyPropertyChanged, new()
    {
        private readonly ObservableCollection<ElementType> m_VectorDataList;

        public ObservableCollection<ElementType> VectorList { get { return m_VectorDataList; } }

        public VectorDataVM()
        {
            m_VectorDataList = new ObservableCollection<ElementType>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using CetiopterNethandle;

namespace AVXCalculator
{
    public abstract class NamedDataBase
    {
        protected string m_Name;

        public string Name
        {
            get { return m_Name; }
        }

        public NamedDataBase(string name)
        {
            m_Name = name;
        }

        public abstract override string ToString();
    }

    public class DoubleScalarData : NamedDataBase, INotifyPropertyChanged
    {
        private double m_Data;

        //only actually gets called for a name change where we previously had no name
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public DoubleScalarData() : base("")
        {
            m_Data = default;
        }

        public new string Name
        {
            get { return base.Name; }
            set
            {
                //work out if this is a first name
                bool firstName = base.m_Name.Length == 0;

                base.m_Name = value;

                //if we're changing name for the first time, we need to notify the collection
                if (firstName)
                {
                    NotifyPropertyChanged();
                }
            }
        }
        public double Data { get { return m_Data; } set { m_Data = value; } }

        public override string ToString()
        {
            return m_Data.ToString();
        }
    }

    public class DoubleVectorData : NamedDataBase, INotifyPropertyChanged
    {
        private DoubleVector m_Vector;

        //only actually gets called for a name change where we previously had no name
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public DoubleVectorData() : base("")
        {
            m_Vector = new DoubleVector();
        }

        public new string Name
        {
            get { return base.Name; }
            set
            {
                //work out if this is a first name
                bool firstName = base.m_Name.Length == 0;

                base.m_Name = value;

                //if we're changing name for the first time, we need to notify the collection
                if (firstName)
                {
                    NotifyPropertyChanged();
                }
            }
        }
        public DoubleVector Vector { get { return m_Vector; } set { m_Vector = value; } }
        public double X { get { return m_Vector.X; } set { m_Vector.X = value; } }
        public double Y { get { return m_Vector.Y; } set { m_Vector.Y = value; } }
        public double Z { get { return m_Vector.Z; } set { m_Vector.Z = value; } }
        public double W { get { return m_Vector.W; } set { m_Vector.W = value; } }

        public override string ToString()
        {
            string prepend = Name.Length > 0 ? string.Format("{0}: ", Name) : "";
            return string.Format("{0}({1:g4}, {2:g4}, {3:g4}, {4:g4})", prepend, X, Y, Z, W);
        }
    }

    public class IntVectorData : NamedDataBase, INotifyPropertyChanged
    {
        private IntVector m_Vector;

        //only actually gets called for a name change where we previously had no name
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public IntVectorData() : base("")
        {
            m_Vector = new IntVector();
        }

        public new string Name
        {
            get { return base.Name; }
            set
            {
                //work out if this is a first name
                bool firstName = base.m_Name.Length == 0;

                base.m_Name = value;

                //if we're changing name for the first time, we need to notify the collection
                if (firstName)
                {
                    NotifyPropertyChanged();
                }
            }
        }
        public IntVector Vector { get { return m_Vector; } set { m_Vector = value; } }
        public long X { get { return m_Vector.X; } set { m_Vector.X = value; } }
        public long Y { get { return m_Vector.Y; } set { m_Vector.Y = value; } }
        public long Z { get { return m_Vector.Z; } set { m_Vector.Z = value; } }
        public long W { get { return m_Vector.W; } set { m_Vector.W = value; } }

        public override string ToString()
        {
            string prepend = Name.Length > 0 ? string.Format("{0}: ", Name) : "";
            return string.Format("{0}({1}, {2}, {3}, {4})", prepend, X, Y, Z, W);
        }
    }
}

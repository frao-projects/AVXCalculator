﻿using System.Collections;
using System.Diagnostics;
using System.Windows.Controls;
using System.Globalization;
using System.Windows;

namespace AVXCalculator
{
    // NOTE: Everything in this class is basically an extended hack bc validation rules cannot
    // contain simple data, from the rest of the program, for some reason unknown to me. Thus,
    // we undertake a series of hacks, to allow us to determine if a name has been used before.
    //
    // This is further complicated by the fact that our vector list has two possible types

    public class BindingProxy : Freezable
    {
        protected override Freezable CreateInstanceCore()
        {
            return new BindingProxy();
        }

        public object Data
        {
            get { return (object)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(object), typeof(BindingProxy), new PropertyMetadata(null));
    }

    public class ListWrapper : DependencyObject
    {
        public static readonly DependencyProperty ListProperty = DependencyProperty.Register("List", typeof(IList), typeof(ListWrapper), new FrameworkPropertyMetadata(null));

        public IList List { get { return (IList)GetValue(ListProperty); } set { SetValue(ListProperty, value); } }
    }

    public class ValidNameRule : ValidationRule
    {
        public ListWrapper ListWrapper { get; set; }

        public ValidNameRule()
        { }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            Debug.Assert(value is string);
            string newName = (string)value;

            if (newName.Length == 0)
            {
                return new ValidationResult(false, "Cannot have empty name");
            }

            if(ListWrapper.List == null)
            {
                return new ValidationResult(false, "No list to check!");
            }

            IEnumerator iter = ListWrapper.List.GetEnumerator();

            if (!iter.MoveNext())
            {
                //we had no existing vectors, so all non-null vector names are valid
                return new ValidationResult(true, "");
            }

            bool validName = true;

            if (iter.Current is IntVectorData)
            {
                iter.Reset();

                while (iter.MoveNext())
                {
                    Debug.Assert(iter.Current is IntVectorData);

                    if (((IntVectorData)iter.Current).Name == newName)
                    {
                        validName = false;
                        break;
                    }
                }
            }
            else
            {
                iter.Reset();

                while (iter.MoveNext())
                {
                    Debug.Assert(iter.Current is DoubleVectorData);

                    if (((DoubleVectorData)iter.Current).Name == newName)
                    {
                        validName = false;
                        break;
                    }
                }
            }

            return new ValidationResult(validName, "Name was already used. Name must be unique");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using CetiopterNethandle;
using AVXCalculator.Commands;

namespace AVXCalculator
{
    public enum ComputationFunction
    {
        Add,
        Subtract,
        Multiply,
        Divide,
        Remainder,
        And,
        Or,
        Xor,
        Not,
        Sqrt,
        Negate,
        Scale,
        Sin,
        Cos,
        Tan,
        ArcSin,
        ArcCos,
        ArcTan,
        Exp,
        Pow,
        Log,
        Abs,
        Min,
        Max,
        Round,
        Dot,
        Cross,
        Length,
        LengthSq,
        Normalise,
        ZeroVec,
        OneVec,
        AllBitsVec,
        EVec,
        EpsilonVec,
        InfVec,
        Convert
    }


    public class ComputationVariantDesc
    {
        //some names of variants that we need to access in the calculation function
        public const string BaseEName = "Base-e";
        public const string Base2Name = "Base-2";
        public const string Base10Name = "Base-10";
        public const string RoundName = "Standard (bankers) round";
        public const string FloorName = "Floor";
        public const string CeilingName = "Ceiling";
        public const string TruncateName = "Truncate";
        public const string Dim2Name = "2-Dimensional";
        public const string Dim3Name = "3-Dimensional";
        public const string Dim4Name = "4-Dimensional";
        public const string DoubleVecName = "Double vector(s)";
        public const string IntVecName = "Int vector(s)";
        public const string ConvertToIntName = "Convert to int vector";
        public const string ConvertToDoubleName = "Convert to double vector";

        //full name of the variant of this computation. eg: for Add<int vector> it would be "int vector", Divide<double vector> would be "double vector" etc
        private readonly string m_VariantType;
        //stores the type of each argument to the computation
        private readonly List<Type> m_ArgType;

        public string Name { get { return m_VariantType; } }

        public Type ArgumentOneType { get { return m_ArgType.Count > 0 ? m_ArgType[0] : null; } }
        public Type ArgumentTwoType { get { return m_ArgType.Count > 1 ? m_ArgType[1] : null; } }

        public int NumArguments { get { return m_ArgType.Count; } }

        //variantType must not be null or empty
        public ComputationVariantDesc(string variantType, params Type[] argList)
        {
            Debug.Assert(variantType != null);
            Debug.Assert(variantType.Length > 0);

            m_VariantType = variantType;
            m_ArgType = new List<Type>();

            foreach (Type argType in argList)
            {
                m_ArgType.Add(argType);
            }
        }
    }
    public class ComputationVariant_IMPL<TR> : ComputationVariantDesc
    {
        Func<TR> m_Function;

        public ComputationVariant_IMPL(string variantType, Func<TR> func) : base(variantType)
        {
            m_Function = func;
        }

        public TR Calculate()
        {
            return m_Function.Invoke();
        }
    }
    public class ComputationVariant_IMPL<T1, TR> : ComputationVariantDesc
    {
        Func<T1, TR> m_Function;

        public ComputationVariant_IMPL(string variantType, Func<T1, TR> func) : base(variantType, typeof(T1))
        {
            m_Function = func;
        }

        public TR Calculate(T1 arg1)
        {
            return m_Function.Invoke(arg1);
        }
    }
    public class ComputationVariant_IMPL<T1, T2, TR> : ComputationVariantDesc
    {
        Func<T1, T2, TR> m_Function;

        public ComputationVariant_IMPL(string variantType, Func<T1, T2, TR> func) : base(variantType, typeof(T1), typeof(T2))
        {
            m_Function = func;
        }

        public TR Calculate(T1 arg1, T2 arg2)
        {
            return m_Function.Invoke(arg1, arg2);
        }
    }

    public class ComputationSpecModel
    {
        private Dictionary<ComputationFunction, ObservableCollection<ComputationVariantDesc>> m_ValidVariants;
        private ComputationFunction m_CurrentFunction;
        private ComputationVariantDesc m_FunctionVariant;

        private void SetupVariants()
        {
            Type dblVecType = typeof(DoubleVectorData),
                intVecType = typeof(IntVectorData),
                dblScalarType = typeof(DoubleScalarData);

            //first create the dictionary itself
            m_ValidVariants = new Dictionary<ComputationFunction, ObservableCollection<ComputationVariantDesc>>();

            //add 'add' function variants
            m_ValidVariants.Add(ComputationFunction.Add, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Add].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.Add(arg1.Vector, arg2.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Add].Add(new ComputationVariant_IMPL<IntVectorData, IntVectorData, IntVectorData>(
                ComputationVariantDesc.IntVecName,
                new Func<IntVectorData, IntVectorData, IntVectorData>(
                    (arg1, arg2) => new IntVectorData { Vector = IntVector.Add(arg1.Vector, arg2.Vector) })
                ));

            //add 'subtract' function variants
            m_ValidVariants.Add(ComputationFunction.Subtract, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Subtract].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.Subtract(arg1.Vector, arg2.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Subtract].Add(new ComputationVariant_IMPL<IntVectorData, IntVectorData, IntVectorData>(
                ComputationVariantDesc.IntVecName,
                new Func<IntVectorData, IntVectorData, IntVectorData>(
                    (arg1, arg2) => new IntVectorData { Vector = IntVector.Subtract(arg1.Vector, arg2.Vector) })
                ));

            //add 'multiply' function variants
            m_ValidVariants.Add(ComputationFunction.Multiply, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Multiply].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.Multiply(arg1.Vector, arg2.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Multiply].Add(new ComputationVariant_IMPL<IntVectorData, IntVectorData, IntVectorData>(
                ComputationVariantDesc.IntVecName,
                new Func<IntVectorData, IntVectorData, IntVectorData>(
                    (arg1, arg2) => new IntVectorData { Vector = IntVector.Multiply(arg1.Vector, arg2.Vector) })
                ));

            //add 'divide' function variants
            m_ValidVariants.Add(ComputationFunction.Divide, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Divide].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.Divide(arg1.Vector, arg2.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Divide].Add(new ComputationVariant_IMPL<IntVectorData, IntVectorData, IntVectorData>(
                ComputationVariantDesc.IntVecName,
                new Func<IntVectorData, IntVectorData, IntVectorData>(
                    (arg1, arg2) => new IntVectorData { Vector = IntVector.Divide(arg1.Vector, arg2.Vector) })
                ));

            //add 'remainder' function variants
            m_ValidVariants.Add(ComputationFunction.Remainder, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Remainder].Add(new ComputationVariant_IMPL<IntVectorData, IntVectorData, IntVectorData>(
                ComputationVariantDesc.IntVecName,
                new Func<IntVectorData, IntVectorData, IntVectorData>(
                    (arg1, arg2) => new IntVectorData { Vector = IntVector.Remainder(arg1.Vector, arg2.Vector) })
                ));

            //add 'and' function variants
            m_ValidVariants.Add(ComputationFunction.And, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.And].Add(new ComputationVariant_IMPL<IntVectorData, IntVectorData, IntVectorData>(
                ComputationVariantDesc.IntVecName,
                new Func<IntVectorData, IntVectorData, IntVectorData>(
                    (arg1, arg2) => new IntVectorData { Vector = IntVector.And(arg1.Vector, arg2.Vector) })
                ));

            //add 'or' function variants
            m_ValidVariants.Add(ComputationFunction.Or, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Or].Add(new ComputationVariant_IMPL<IntVectorData, IntVectorData, IntVectorData>(
                ComputationVariantDesc.IntVecName,
                new Func<IntVectorData, IntVectorData, IntVectorData>(
                    (arg1, arg2) => new IntVectorData { Vector = IntVector.Or(arg1.Vector, arg2.Vector) })
                ));

            //add 'xor' function variants
            m_ValidVariants.Add(ComputationFunction.Xor, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Xor].Add(new ComputationVariant_IMPL<IntVectorData, IntVectorData, IntVectorData>(
                ComputationVariantDesc.IntVecName,
                new Func<IntVectorData, IntVectorData, IntVectorData>(
                    (arg1, arg2) => new IntVectorData { Vector = IntVector.Xor(arg1.Vector, arg2.Vector) })
                ));

            //add 'not' function variants
            m_ValidVariants.Add(ComputationFunction.Not, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Not].Add(new ComputationVariant_IMPL<IntVectorData, IntVectorData>(
                ComputationVariantDesc.IntVecName,
                new Func<IntVectorData, IntVectorData>(
                    (arg) => new IntVectorData { Vector = IntVector.Not(arg.Vector) })
                ));

            //add 'sqrt' function variants
            m_ValidVariants.Add(ComputationFunction.Sqrt, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Sqrt].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Sqrt(arg.Vector) })
                ));

            //add 'negate' function variants
            m_ValidVariants.Add(ComputationFunction.Negate, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Negate].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Negate(arg.Vector) })
                ));

            //add 'scale' function variants
            m_ValidVariants.Add(ComputationFunction.Scale, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Scale].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleScalarData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleScalarData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.Scale(arg1.Vector, arg2.Data) })
                ));

            //add 'sin' function variants
            m_ValidVariants.Add(ComputationFunction.Sin, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Sin].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Sin(arg.Vector) })
                ));

            //add 'cos' function variants
            m_ValidVariants.Add(ComputationFunction.Cos, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Cos].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Cos(arg.Vector) })
                ));

            //add 'tan' function variants
            m_ValidVariants.Add(ComputationFunction.Tan, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Tan].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Tan(arg.Vector) })
                ));

            //add 'asin' function variants
            m_ValidVariants.Add(ComputationFunction.ArcSin, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.ArcSin].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.ArcSin(arg.Vector) })
                ));

            //add 'acos' function variants
            m_ValidVariants.Add(ComputationFunction.ArcCos, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.ArcCos].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.ArcCos(arg.Vector) })
                ));

            //add 'atan' function variants
            m_ValidVariants.Add(ComputationFunction.ArcTan, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.ArcTan].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                "Single argument",
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.ArcTan(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.ArcTan].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                "Two argument",
                new Func<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.ArcTan2(arg1.Vector, arg2.Vector) })
                ));

            //add 'exp' function variants
            m_ValidVariants.Add(ComputationFunction.Exp, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Exp].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.BaseEName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Exp(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Exp].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Base2Name,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Exp2(arg.Vector) })
                ));

            //add 'pow' function variants
            m_ValidVariants.Add(ComputationFunction.Pow, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Pow].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.Pow(arg1.Vector, arg2.Vector) })
                ));

            //add 'log' function variants
            m_ValidVariants.Add(ComputationFunction.Log, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Log].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.BaseEName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Log(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Log].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Base2Name,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Log2(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Log].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Base10Name,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Log10(arg.Vector) })
                ));

            //add 'abs' function variants
            m_ValidVariants.Add(ComputationFunction.Abs, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Abs].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Abs(arg.Vector) })
                ));

            //add 'min' function variants
            m_ValidVariants.Add(ComputationFunction.Min, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Min].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.Min(arg1.Vector, arg2.Vector) })
                ));

            //add 'max' function variants
            m_ValidVariants.Add(ComputationFunction.Max, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Max].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.Max(arg1.Vector, arg2.Vector) })
                ));

            //add 'dot' function variants
            m_ValidVariants.Add(ComputationFunction.Dot, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Dot].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim2Name,
                new Func<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.Dot2D(arg1.Vector, arg2.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Dot].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim3Name,
                new Func<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.Dot3D(arg1.Vector, arg2.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Dot].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim4Name,
                new Func<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.Dot4D(arg1.Vector, arg2.Vector) })
                ));

            //add 'cross' function variants
            m_ValidVariants.Add(ComputationFunction.Cross, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Cross].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim3Name,
                new Func<DoubleVectorData, DoubleVectorData, DoubleVectorData>(
                    (arg1, arg2) => new DoubleVectorData { Vector = DoubleVector.Cross3D(arg1.Vector, arg2.Vector) })
                ));

            //add 'length' function variants
            m_ValidVariants.Add(ComputationFunction.Length, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Length].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim2Name,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Length2D(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Length].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim3Name,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Length3D(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Length].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim4Name,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Length4D(arg.Vector) })
                ));

            //add 'lengthSq' function variants
            m_ValidVariants.Add(ComputationFunction.LengthSq, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.LengthSq].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim2Name,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.LengthSq2D(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.LengthSq].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim3Name,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.LengthSq3D(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.LengthSq].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim4Name,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.LengthSq4D(arg.Vector) })
                ));

            //add 'normalise' function variants
            m_ValidVariants.Add(ComputationFunction.Normalise, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Normalise].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim2Name,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Normalise2D(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Normalise].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim3Name,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Normalise3D(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Normalise].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.Dim4Name,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Normalise4D(arg.Vector) })
                ));

            //add 'round' function variants
            m_ValidVariants.Add(ComputationFunction.Round, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Round].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.RoundName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Round(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Round].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.TruncateName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Truncate(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Round].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.FloorName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Floor(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Round].Add(new ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData>(
                ComputationVariantDesc.CeilingName,
                new Func<DoubleVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = DoubleVector.Ceiling(arg.Vector) })
                ));

            //add 'zero' function variants
            m_ValidVariants.Add(ComputationFunction.ZeroVec, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.ZeroVec].Add(new ComputationVariant_IMPL<DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData>(
                    () => new DoubleVectorData { Vector = DoubleVector.Zero() })
                ));
            m_ValidVariants[ComputationFunction.ZeroVec].Add(new ComputationVariant_IMPL<IntVectorData>(
                ComputationVariantDesc.IntVecName,
                new Func<IntVectorData>(
                    () => new IntVectorData { Vector = IntVector.Zero() })
                ));

            //add 'one' function variants
            m_ValidVariants.Add(ComputationFunction.OneVec, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.OneVec].Add(new ComputationVariant_IMPL<DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData>(
                    () => new DoubleVectorData { Vector = DoubleVector.One() })
                ));
            m_ValidVariants[ComputationFunction.OneVec].Add(new ComputationVariant_IMPL<IntVectorData>(
                ComputationVariantDesc.IntVecName,
                new Func<IntVectorData>(
                    () => new IntVectorData { Vector = IntVector.One() })
                ));

            //add 'all bits' function variants
            m_ValidVariants.Add(ComputationFunction.AllBitsVec, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.AllBitsVec].Add(new ComputationVariant_IMPL<IntVectorData>(
                ComputationVariantDesc.IntVecName,
                new Func<IntVectorData>(
                    () => new IntVectorData { Vector = IntVector.AllBits() })
                ));

            //add 'e' function variants
            m_ValidVariants.Add(ComputationFunction.EVec, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.EVec].Add(new ComputationVariant_IMPL<DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData>(
                    () => new DoubleVectorData { Vector = DoubleVector.Exp(DoubleVector.One()) })
                ));

            //add 'epsilon' function variants
            m_ValidVariants.Add(ComputationFunction.EpsilonVec, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.EpsilonVec].Add(new ComputationVariant_IMPL<DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData>(
                    () => new DoubleVectorData { Vector = DoubleVector.Epsilon() })
                ));

            //add 'inf' function variants
            m_ValidVariants.Add(ComputationFunction.InfVec, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.InfVec].Add(new ComputationVariant_IMPL<DoubleVectorData>(
                ComputationVariantDesc.DoubleVecName,
                new Func<DoubleVectorData>(
                    () => new DoubleVectorData { Vector = DoubleVector.Infinity() })
                ));

            //add 'convert' function variants
            m_ValidVariants.Add(ComputationFunction.Convert, new ObservableCollection<ComputationVariantDesc>());
            m_ValidVariants[ComputationFunction.Convert].Add(new ComputationVariant_IMPL<DoubleVectorData, IntVectorData>(
                ComputationVariantDesc.ConvertToIntName,
                new Func<DoubleVectorData, IntVectorData>(
                    (arg) => new IntVectorData { Vector = DoubleVector.ToIntVector(arg.Vector) })
                ));
            m_ValidVariants[ComputationFunction.Convert].Add(new ComputationVariant_IMPL<IntVectorData, DoubleVectorData>(
                ComputationVariantDesc.ConvertToDoubleName,
                new Func<IntVectorData, DoubleVectorData>(
                    (arg) => new DoubleVectorData { Vector = IntVector.ToDoubleVector(arg.Vector) })
                ));
        }

        public ComputationSpecModel()
        {
            SetupVariants();

            m_CurrentFunction = ComputationFunction.Add;

            SetDefaultChoices();
        }

        public ComputationFunction CurrentFunction
        {
            get { return m_CurrentFunction; }
            set
            {
                m_CurrentFunction = value;
                SetDefaultChoices();
            }
        }
        public ComputationVariantDesc FunctionVariant
        {
            get { return m_FunctionVariant; }
            set
            {
                m_FunctionVariant = value;
            }
        }

        public NamedDataBase ArgumentOne { get; set; }
        public NamedDataBase ArgumentTwo { get; set; }

        public string ResultName { get; set; }
        public bool UseResult { get; set; }
        public ObservableCollection<ComputationVariantDesc> ValidVariants
        { get { return m_ValidVariants[m_CurrentFunction]; } }

        //sets all choices (variant etc) to defaults, for this function type
        public void SetDefaultChoices()
        {
            //get the list of valid variants
            bool validFunc = m_ValidVariants.TryGetValue(m_CurrentFunction, out ObservableCollection<ComputationVariantDesc> variantList);
            Debug.Assert(validFunc);
            Debug.Assert(variantList.Count > 0);

            m_FunctionVariant = variantList[0];

            ArgumentOne = null;
            ArgumentTwo = null;

            ResultName = "";
            UseResult = true;
        }
    }

    public class ComputationSpecVM : INotifyPropertyChanged
    {
        private readonly CalculateCommand m_CalcCommand;
        private readonly ComputationSpecModel m_Model;
        private readonly ComboBox m_ArgOneChoiceBox;
        private readonly ComboBox m_ArgTwoChoiceBox;
        private string m_ArgTwoNumber;

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private static void SetArgumentListBinding(ComboBox argBox, Type argType)
        {
            if ((argType != null) && (argType != typeof(DoubleScalarData)))
            {
                Binding sourceBinding = new Binding();

                if (argType == typeof(IntVectorData))
                {
                    sourceBinding.Path = new PropertyPath("IntVectorList.VectorList");
                }
                else
                {
                    Debug.Assert(argType == typeof(DoubleVectorData));
                    sourceBinding.Path = new PropertyPath("DblVectorList.VectorList");
                }

                BindingOperations.SetBinding(argBox, ListView.ItemsSourceProperty, sourceBinding);
            }
        }

        public string Name { get { return m_Model.CurrentFunction.ToString(); } }
        public string ArgumentOneTypeName
        {
            get
            {
                Type arg1Type = m_Model?.FunctionVariant?.ArgumentOneType;
                return (arg1Type != null) ? arg1Type.Name : "n/a";
            }
        }
        public string ArgumentTwoTypeName
        {
            get
            {
                Type arg2Type = m_Model?.FunctionVariant?.ArgumentTwoType;
                return (arg2Type != null) ? arg2Type.Name : "n/a";
            }
        }

        public bool ArgumentOneEnabled
        {
            get
            {
                Type arg1Type = m_Model?.FunctionVariant?.ArgumentOneType;

                return arg1Type != null;
            }
        }
        public bool ArgumentTwoEnabled
        {
            get
            {
                Type arg2Type = m_Model?.FunctionVariant?.ArgumentTwoType;

                return arg2Type != null;
            }
        }
        public uint NumExpectedArguments
        {
            get
            {
                int? result = m_Model?.FunctionVariant?.NumArguments;

                return (uint)(result != null ? result.Value : 0);
            }
        }

        public NamedDataBase ArgumentOne
        {
            get { return m_Model.ArgumentOne; }
            set
            {
                m_Model.ArgumentOne = value;
                NotifyPropertyChanged();
            }
        }
        public NamedDataBase ArgumentTwo
        {
            get { return m_Model.ArgumentTwo; }
            set
            {
                m_Model.ArgumentTwo = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("ArgumentTwoValue");
            }
        }

        public string ArgumentTwoValue
        {
            get
            {
                return m_ArgTwoNumber;
            }
            set
            {
                m_ArgTwoNumber = value;

                //set actual data, if string is in a valid format
                if (m_Model.ArgumentTwo == null)
                {
                    if (double.TryParse(value, out double parsedValue))
                    {
                        m_Model.ArgumentTwo = new DoubleScalarData
                        {
                            Name = value,
                            Data = parsedValue
                        };

                        NotifyPropertyChanged("ArgumentTwoValue");
                    }
                }
                else if (m_Model.ArgumentTwo is DoubleScalarData doubleData)
                {
                    if (double.TryParse(value, out double parsedValue))
                    {
                        //set both data and name, to the value. Name is so that the data is valid
                        doubleData.Data = parsedValue;
                        doubleData.Name = value;
                    }
                    else
                    {
                        //set name to empty, so that we know the data is invalid
                        doubleData.Name = "";
                    }

                    NotifyPropertyChanged("ArgumentTwoValue");
                }

                NotifyPropertyChanged();
            }
        }



        public ObservableCollection<ComputationVariantDesc> VariantChoices { get { return m_Model.ValidVariants; } }
        public ComputationVariantDesc SelectedVariant
        {
            get { return m_Model.FunctionVariant; }
            set
            {
                m_Model.FunctionVariant = value;

                NotifyPropertyChanged();

                SetArgumentListBinding(m_ArgOneChoiceBox, m_Model.FunctionVariant?.ArgumentOneType);
                SetArgumentListBinding(m_ArgTwoChoiceBox, m_Model.FunctionVariant?.ArgumentTwoType);

                //have to explicitly inform that the dependant properties may have changed
                NotifyPropertyChanged("ArgumentOne");
                NotifyPropertyChanged("ArgumentTwo");
                NotifyPropertyChanged("ArgumentOneTypeName");
                NotifyPropertyChanged("ArgumentTwoTypeName");
                NotifyPropertyChanged("ArgumentOneEnabled");
                NotifyPropertyChanged("ArgumentTwoEnabled");
                NotifyPropertyChanged("ArgumentTwoSelection");
                NotifyPropertyChanged("ArgumentTwoTyped");
                NotifyPropertyChanged("ArgumentTwoValue");
            }
        }

        public ComputationFunction CurrentFunction
        {
            get { return m_Model.CurrentFunction; }
            set
            {
                m_Model.CurrentFunction = value;
                NotifyPropertyChanged();

                //have to explicitly inform that the dependant properties may have changed 
                NotifyPropertyChanged("Name");
                NotifyPropertyChanged("VariantChoices");

                //have to manually reset the selected item
                m_Model.SetDefaultChoices();
                NotifyPropertyChanged("SelectedVariant");
                NotifyPropertyChanged("ArgumentOne");
                NotifyPropertyChanged("ArgumentTwo");
                NotifyPropertyChanged("ArgumentOneTypeName");
                NotifyPropertyChanged("ArgumentTwoTypeName");
                NotifyPropertyChanged("ArgumentOneEnabled");
                NotifyPropertyChanged("ArgumentTwoEnabled");
                NotifyPropertyChanged("ArgumentTwoSelection");
                NotifyPropertyChanged("ArgumentTwoTyped");
                NotifyPropertyChanged("ArgumentTwoValue");
                NotifyPropertyChanged("ResultName");
                NotifyPropertyChanged("UseResult");

                SetArgumentListBinding(m_ArgOneChoiceBox, m_Model.FunctionVariant?.ArgumentOneType);
                SetArgumentListBinding(m_ArgTwoChoiceBox, m_Model.FunctionVariant?.ArgumentTwoType);

            }
        }
        public string ResultName
        {
            get { return m_Model.ResultName; }
            set
            {
                m_Model.ResultName = value;
                NotifyPropertyChanged();
            }
        }
        public bool UseResult
        {
            get { return m_Model.UseResult; }
            set
            {
                m_Model.UseResult = value;
                NotifyPropertyChanged();
            }
        }


        //returns Visible when we have an argument that should be selected (from a list), and collapsed otherwise
        public Visibility ArgumentTwoSelection
        {
            get
            {
                Type argType = m_Model?.FunctionVariant?.ArgumentTwoType;
                return ((argType == null) || (argType != typeof(DoubleScalarData))) ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        //returns Visible when we have an argument that should be typed in manually (like a single double), and collapsed otherwise
        public Visibility ArgumentTwoTyped
        {
            get
            {
                Type argType = m_Model?.FunctionVariant?.ArgumentTwoType;
                return ((argType != null) && (argType == typeof(DoubleScalarData))) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public CalculateCommand CalculationCommand { get { return m_CalcCommand; } }

        public ComputationSpecVM(ResultsVM resultsVM, ComboBox arg1Box, ComboBox arg2Box)
        {
            m_CalcCommand = new CalculateCommand(resultsVM);

            m_Model = new ComputationSpecModel();

            m_ArgOneChoiceBox = arg1Box;
            m_ArgTwoChoiceBox = arg2Box;
        }
    }
}

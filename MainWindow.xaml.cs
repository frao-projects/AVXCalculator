﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AVXCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowVM ViewModel;

        public MainWindow()
        {
            InitializeComponent();

            //get DataGrid object
            object gridObject = FindName("StoredVectorsGrid");
            Debug.Assert(gridObject != null);
            Debug.Assert(gridObject is DataGrid);

            //get arg1 ComboBox object
            object arg1Object = FindName("Arg1Box");
            Debug.Assert(arg1Object != null);
            Debug.Assert(arg1Object is ComboBox);

            //get arg2 ComboBox object
            object arg2Object = FindName("Arg2Box");
            Debug.Assert(arg2Object != null);
            Debug.Assert(arg2Object is ComboBox);

            ViewModel = new MainWindowVM((DataGrid)gridObject, (ComboBox)arg1Object, (ComboBox)arg2Object);
            
            //set basic window data context
            DataContext = ViewModel;
        }
    }
}

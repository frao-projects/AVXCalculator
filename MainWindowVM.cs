﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using AVXCalculator.Commands;

namespace AVXCalculator
{
    public class MainWindowVM : INotifyPropertyChanged
    {
        private CalculationChoiceCommand m_FuncChoiceCommand;
        private PickVectorListCommand m_VectorListChoiceCommand;
        private readonly DataGrid m_VectorGrid;
        private bool m_IntGridChosen;

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void AddVector(NamedDataBase newVector)
        {
            if (newVector is DoubleVectorData doubleVec)
            {
                IEnumerator iter = DblVectorList.VectorList.GetEnumerator();

                while (iter.MoveNext())
                {
                    if (((DoubleVectorData)iter.Current).Name == doubleVec.Name)
                    {
                        ((DoubleVectorData)iter.Current).Vector = doubleVec.Vector;
                        return;
                    }
                }

                //if we get here
                DblVectorList.VectorList.Add(doubleVec);
            }
            else if (newVector is IntVectorData intVec)
            {
                IEnumerator iter = IntVectorList.VectorList.GetEnumerator();

                while (iter.MoveNext())
                {
                    if (((IntVectorData)iter.Current).Name == intVec.Name)
                    {
                        ((IntVectorData)iter.Current).Vector = intVec.Vector;
                        return;
                    }
                }

                IntVectorList.VectorList.Add(intVec);
            }
        }

        //a value of false for m_IntGridChosen implies a double list
        private void SetVectorList()
        {
            Binding sourceBinding = new Binding();

            if (m_IntGridChosen)
            {
                sourceBinding.Path = new PropertyPath("IntVectorList.VectorList");
            }
            else
            {
                sourceBinding.Path = new PropertyPath("DblVectorList.VectorList");
            }

            BindingOperations.SetBinding(m_VectorGrid, ListView.ItemsSourceProperty, sourceBinding);
        }

        public VectorDataVM<DoubleVectorData> DblVectorList { get; }
        public VectorDataVM<IntVectorData> IntVectorList { get; }
        public ComputationSpecVM BuildingComputation { get; }
        public ResultsVM Results { get; }
        public bool IntChosen
        {
            get { return m_IntGridChosen; }
            set
            {
                m_IntGridChosen = value;
                SetVectorList();
                NotifyPropertyChanged();
                NotifyPropertyChanged("ChosenList");
            }
        }

        public IList ChosenList
        {
            get
            {
                if (m_IntGridChosen)
                {
                    return IntVectorList.VectorList;
                }
                else { return DblVectorList.VectorList; }
            }
        }
        public CalculationChoiceCommand FuncChoiceCommand { get { return m_FuncChoiceCommand; } set { m_FuncChoiceCommand = value; } }
        public PickVectorListCommand PickListCommand { get { return m_VectorListChoiceCommand; } set { m_VectorListChoiceCommand = value; } }

        public MainWindowVM(DataGrid vectorDisplayGrid, ComboBox arg1Box, ComboBox arg2Box)
        {
            //create double and int vector stores
            DblVectorList = new VectorDataVM<DoubleVectorData>();
            IntVectorList = new VectorDataVM<IntVectorData>();

            m_VectorGrid = vectorDisplayGrid;
            m_IntGridChosen = false;

            //add results window VM
            Results = new ResultsVM(delegate (NamedDataBase newVec)
            {
                if (newVec.Name.Length > 0) { AddVector(newVec); }
            });

            //add Building command
            BuildingComputation = new ComputationSpecVM(Results, arg1Box, arg2Box);

            m_FuncChoiceCommand = new CalculationChoiceCommand(BuildingComputation);
            m_VectorListChoiceCommand = new PickVectorListCommand(this);

            SetVectorList();
        }

    }
}

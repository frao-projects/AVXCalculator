﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace AVXCalculator.Commands
{
    public class CalculationChoiceCommand : ICommand
    {
        private readonly ComputationSpecVM m_SpecVM;

        public CalculationChoiceCommand(ComputationSpecVM spec)
        {
            m_SpecVM = spec;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            Debug.Assert(parameter != null);
            Debug.Assert(parameter is string);

            m_SpecVM.CurrentFunction = (ComputationFunction)Enum.Parse(typeof(ComputationFunction), (string)parameter);
        }
        public bool CanExecute(object parameter)
        {
            Debug.Assert(parameter != null);
            Debug.Assert(parameter is string);

            //ie: we can change functions, if we have selected a different function
            return m_SpecVM.CurrentFunction != (ComputationFunction)Enum.Parse(typeof(ComputationFunction), (string)parameter);
        }
    }
}

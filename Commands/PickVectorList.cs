﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace AVXCalculator.Commands
{
    public class PickVectorListCommand : ICommand
    {
        private readonly MainWindowVM m_MainWindowVM;

        public PickVectorListCommand(MainWindowVM mainWindowVM)
        {
            m_MainWindowVM = mainWindowVM;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            Debug.Assert(parameter != null);
            Debug.Assert(parameter is string);

            m_MainWindowVM.IntChosen = bool.Parse((string)parameter);
        }
        public bool CanExecute(object parameter)
        {
            Debug.Assert(parameter != null);
            Debug.Assert(parameter is string);

            return m_MainWindowVM.IntChosen != bool.Parse((string)parameter);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Input;

using CetiopterNethandle;

namespace AVXCalculator.Commands
{
    public class CalculateCommand : ICommand
    {
        private readonly ResultsVM m_Results;

        public CalculateCommand(ResultsVM results)
        {
            m_Results = results;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        //returns true if the command was recognised, and arguments were of the right types
        private bool Calculate(string commandName, ComputationVariantDesc variant, out NamedDataBase result, NamedDataBase[] argList)
        {
            result = new DoubleVectorData();

            if (argList.Length < variant.NumArguments)
            {
                result = new DoubleVectorData();
                return false;
            }

            try
            {
                switch (Enum.Parse(typeof(ComputationFunction), commandName))
                {
                    case ComputationFunction.Add:
                    case ComputationFunction.Subtract:
                    case ComputationFunction.Multiply:
                    case ComputationFunction.Divide:
                        {
                            if ((variant is ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData> vard)
                                && (argList[0] is DoubleVectorData argd1) && (argList[1] is DoubleVectorData argd2))
                            {
                                result = vard.Calculate(argd1, argd2);
                            }
                            else if ((variant is ComputationVariant_IMPL<IntVectorData, IntVectorData, IntVectorData> vari)
                                && (argList[0] is IntVectorData argi1) && (argList[1] is IntVectorData argi2))
                            {
                                result = vari.Calculate(argi1, argi2);
                            }
                            else
                            {
                                throw new InvalidOperationException();
                            }
                        }
                        break;
                    case ComputationFunction.Convert:
                        {
                            if ((variant is ComputationVariant_IMPL<DoubleVectorData, IntVectorData> varir)
                                && (argList[0] is DoubleVectorData argd))
                            {
                                result = varir.Calculate(argd);
                            }
                            else if ((variant is ComputationVariant_IMPL<IntVectorData, DoubleVectorData> vardr)
                                && (argList[0] is IntVectorData argi))
                            {
                                result = vardr.Calculate(argi);
                            }
                            else
                            {
                                throw new InvalidOperationException();
                            }
                        }
                        break;
                    case ComputationFunction.ZeroVec:
                    case ComputationFunction.OneVec:
                        {
                            if (variant is ComputationVariant_IMPL<DoubleVectorData> vard)
                            {
                                result = vard.Calculate();
                            }
                            else if (variant is ComputationVariant_IMPL<IntVectorData> vari)
                            {
                                result = vari.Calculate();
                            }
                            else
                            {
                                throw new InvalidOperationException();
                            }
                        }
                        break;
                    case ComputationFunction.Remainder:
                    case ComputationFunction.And:
                    case ComputationFunction.Or:
                    case ComputationFunction.Xor:
                        {
                            if ((variant is ComputationVariant_IMPL<IntVectorData, IntVectorData, IntVectorData> var)
                                && (argList[0] is IntVectorData arg1) && (argList[1] is IntVectorData arg2))
                            {
                                result = var.Calculate(arg1, arg2);
                            }
                            else
                            {
                                throw new InvalidOperationException();
                            }
                        }
                        break;
                    case ComputationFunction.Not:
                        {
                            if ((variant is ComputationVariant_IMPL<IntVectorData, IntVectorData> var) && (argList[0] is IntVectorData arg))
                            {
                                result = var.Calculate(arg);
                            }
                            else
                            {
                                throw new InvalidOperationException();
                            }
                        }
                        break;
                    case ComputationFunction.AllBitsVec:
                        {
                            if (variant is ComputationVariant_IMPL<IntVectorData> var)
                            {
                                result = var.Calculate();
                            }
                            else
                            {
                                throw new InvalidOperationException();
                            }
                        }
                        break;
                    case ComputationFunction.Pow:
                    case ComputationFunction.Min:
                    case ComputationFunction.Max:
                    case ComputationFunction.Dot:
                    case ComputationFunction.Cross:
                        {
                            if ((variant is ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData> var)
                                && (argList[0] is DoubleVectorData arg1) && (argList[1] is DoubleVectorData arg2))
                            {
                                result = var.Calculate(arg1, arg2);
                            }
                            else
                            {
                                throw new InvalidOperationException();
                            }
                        }
                        break;
                    case ComputationFunction.Scale:
                        {
                            if ((variant is ComputationVariant_IMPL<DoubleVectorData, DoubleScalarData, DoubleVectorData> var)
                                && (argList[0] is DoubleVectorData arg1) && (argList[1] is DoubleScalarData arg2))
                            {
                                result = var.Calculate(arg1, arg2);
                            }
                            else
                            {
                                throw new InvalidOperationException();
                            }
                        }
                        break;
                    case ComputationFunction.ArcTan:
                        {
                            if ((variant is ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData, DoubleVectorData> vard)
                                && (argList[0] is DoubleVectorData argd1) && (argList[1] is DoubleVectorData argd2))
                            {
                                result = vard.Calculate(argd1, argd2);
                            }
                            else if ((variant is ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData> vars)
                                && (argList[0] is DoubleVectorData args))
                            {
                                result = vars.Calculate(args);
                            }
                            else
                            {
                                throw new InvalidOperationException();
                            }
                        }
                        break;
                    case ComputationFunction.Sqrt:
                    case ComputationFunction.Negate:
                    case ComputationFunction.Sin:
                    case ComputationFunction.Cos:
                    case ComputationFunction.Tan:
                    case ComputationFunction.ArcSin:
                    case ComputationFunction.ArcCos:
                    case ComputationFunction.Exp:
                    case ComputationFunction.Log:
                    case ComputationFunction.Abs:
                    case ComputationFunction.Length:
                    case ComputationFunction.LengthSq:
                    case ComputationFunction.Normalise:
                    case ComputationFunction.Round:
                        {
                            if ((variant is ComputationVariant_IMPL<DoubleVectorData, DoubleVectorData> var) && (argList[0] is DoubleVectorData arg))
                            {
                                result = var.Calculate(arg);
                            }
                            else
                            {
                                throw new InvalidOperationException();
                            }
                        }
                        break;
                    case ComputationFunction.EVec:
                    case ComputationFunction.EpsilonVec:
                    case ComputationFunction.InfVec:
                        {
                            if (variant is ComputationVariant_IMPL<DoubleVectorData> var)
                            {
                                result = var.Calculate();
                            }
                            else
                            {
                                throw new InvalidOperationException();
                            }
                        }
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
            catch (Exception)
            {
                result = new DoubleVectorData();
                return false;
            }
            return true;
        }
        private string ParseArgStrings(NamedDataBase[] argList)
        {
            string result = "";

            for (long index = 0; index < argList.Length; ++index)
            {
                result += argList[index].Name;

                if (index < (argList.Length - 1))
                {
                    //ie: if this isn't the last element
                    result += ", ";
                }
            }

            return result;
        }
        private NamedDataBase ApplyName(NamedDataBase data, string name)
        {
            if (data is DoubleScalarData scalar)
            {
                scalar.Name = name;

                return scalar;
            }
            else if (data is DoubleVectorData dVec)
            {
                dVec.Name = name;

                return dVec;
            }
            else if (data is IntVectorData iVec)
            {
                iVec.Name = name;

                return iVec;
            }
            else
            {
                return null;
            }
        }

        public void Execute(object parameter)
        {
            Debug.Assert(parameter != null);
            Debug.Assert(parameter is ComputationSpecVM);
            ComputationSpecVM calcSpec = (ComputationSpecVM)parameter;

            List<NamedDataBase> args = new List<NamedDataBase>();

            switch (calcSpec.NumExpectedArguments)
            {
                case 1:
                    args.Add(calcSpec.ArgumentOne);
                    break;
                case 2:
                    args.Add(calcSpec.ArgumentOne);
                    args.Add(calcSpec.ArgumentTwo);
                    break;
                default:
                    break;
            }

            string argsMessage = ParseArgStrings(args.ToArray());
            bool validCommand = Calculate(calcSpec.Name, calcSpec.SelectedVariant, out NamedDataBase result, args.ToArray());

            //construct string of specified calculation
            string resMessage = calcSpec.UseResult ? string.Format("{0} : = ", calcSpec.ResultName) : "",
                cmdMessage = string.Format("{0}<{1}>({2})", calcSpec.Name, calcSpec.SelectedVariant.Name, argsMessage);

            m_Results.LastComputation = (resMessage + cmdMessage);

            if (validCommand)
            {
                m_Results.ResultValue = ApplyName(result, calcSpec.ResultName);
            }
            else
            {
                m_Results.MarkAsError();
            }
        }
        public bool CanExecute(object parameter)
        {
            if (parameter != null)
            {
                Debug.Assert(parameter is ComputationSpecVM);
                ComputationSpecVM calcSpec = (ComputationSpecVM)parameter;

                uint requiredArgs = calcSpec.NumExpectedArguments,
                    foundArguments = 0;

                if ((calcSpec.ArgumentOne != null) && (calcSpec.ArgumentOne.Name.Length > 0))
                {
                    ++foundArguments;
                }

                if ((calcSpec.ArgumentTwo != null) && (calcSpec.ArgumentTwo.Name.Length > 0))
                {
                    ++foundArguments;
                }

                bool validResult = !calcSpec.UseResult || calcSpec.ResultName.Length > 0,
                    validArgs = requiredArgs == foundArguments;

                return validResult && validArgs && (calcSpec.Name.Length > 0);
            }

            //can't execute with no parameter
            return false;

        }
    }
}
